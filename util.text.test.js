const test = require("tape");
const { format } = require("./util.text");

test('format', function (t) {
  t.plan(2);

  let f1 = format("{{he:capitalize}}{{what.text:space}} when you are {{if sleeping}}sleeping{{end}}{{if isAwake}}awake{{end}}.");

  t.equal(
    f1.toString({ he: "He", what: { text: "knows" }, sleeping: true}),
    "He knows when you are sleeping.",
    "Format with modifiers and tags");

  t.equal(
    f1.toString({ he: "he", what: { text: "knows" }, isAwake: () => true}),
    "He knows when you are awake.",
    "If tag takes executes predicate");
});
