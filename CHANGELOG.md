# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Basic test over format function

### Changed
- Functions are exported using CommonJS

## [0.0.1] - 2020-03-26
### Added
- Create format object given a string format.
- Format an object directly given a key which holds the string format.

[Unreleased]: https://gitlab.com/poisson-me/util-text/compare/v0.0.1...master
[0.0.1]: https://gitlab.com/poisson-me/util-text/-/tags/v0.0.1
