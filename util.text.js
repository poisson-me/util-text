const { isFunction, isObject, isString, has, tail } = require("lodash");

const getAttributeValue = (target, context) => {
  let paths = target.split(".");
  let attribute = context;

  // Follow path down context
  paths.forEach(path => {
    attribute = isObject(attribute) && has(attribute, path)
      ? attribute[path]
      : null;
  });

  return isFunction(attribute) ? attribute() : attribute;
}

class Format {
  constructor(options, contents) {
    this.options = options || [];
    this.contents = contents || [];
    this.string = null;
  }

  buildString (context) {
    return this.contents.map(content => {
      return content instanceof Format
        ? content.toString(context)
        : content;
    }).join("");
  }

  toString (context) {
    return this.buildString(context);
  }
}

class IfFormat extends Format {

  buildString (context) {
    let shouldPrint = getAttributeValue(this.options, context);
    return shouldPrint ? super.buildString(context) : "";
  }
}

const MODIFIERS = {
  "space": function (text) {
    if (isString(text) && text.length > 0) {
      text = " " + text;
    }
    return text;
  },
  "capitalize": function (text) {
    return text.charAt(0).toUpperCase() + text.substring(1);
  },
  "a": function (text) {
    if (text && text.length > 0) {
      let anChars = ['a','e','i','o','u'];
      let an = anChars.includes(text.charAt(0).toLowerCase()) ? "an" : "a";
      return an + " " + text;
    }
    return text;
  },
  "lowercase": function (text) {
    return text.toLowerCase();
  },
  "'s": function (text) {
    let s = text.charAt(text.length-1).toLowerCase() === "s" ? "'" : "'s";
    return text + s;
  },
};

class ReplacementFormat extends Format {

  buildString (context) {
    // Support for nested path, and modifier options
    // foo.bar:baz:bat
    let [target, ...modifiers] = this.options.split(":");
    let result = getAttributeValue(target, context);

    if (result) {
      // Apply modifiers to the result
      modifiers.forEach(modifierName => {
        let modifier = MODIFIERS[modifierName.toLowerCase()];
        if (isFunction(modifier)) {
          result = modifier(result);
        }
      });
    }

    return result;
  }
}

function Tag (name, options, indexStart, tagLength, contentIndexEnd) {
    // Tag information and options
    this.name = name;
    this.options = options;

    // Where the tag starts and ends (so that it can be excluded)
    this.indexStart = indexStart;
    this.indexEnd = indexStart + tagLength;
    // Where the inner content starts and ends (assuming it's a nested tag)
    this.contentIndexStart = this.indexEnd;
    this.contentIndexEnd = contentIndexEnd || this.indexEnd;
    // Any nested child tags
    this.children = [];

    this.addChild = function (child) {
        if (child.name === "end") {
            // For nested tags, track the content and entire area
            this.indexEnd = child.indexEnd;
            this.contentIndexEnd = child.indexStart;
        } else {
            this.children.push(child);
        }
    }

    return this;
}

const TAGS = {
    "root": { format: Format },
    "if": { nest: true, format: IfFormat },
    "end": { end: true },
    "empty": {},
    "replace": {format: ReplacementFormat },
};

function createTag (match) {
    var parts = match[1].split(" ");
    var options = parts;
    var name = (parts[0] || "empty").trim().toLowerCase();
    if (!has(TAGS, name)) {
        name = "replace"; // default to a replacement tag
    } else {
        options = tail(parts); // cut out the tag
    }

    var tag = new Tag(name, options.join(" "), match.index, match[0].length);
    tag.nest = TAGS[name].nest;
    tag.end = TAGS[name].end;
    return tag;
}


// Find any tags in the source string, nesting content for wrapped tags
function extractTags (source) {
    let rootTag = new Tag("root", [], 0, 0, source.length);
    let stack = [];

    let tagRegex = /{{([^}]*)}}/g;
    let match = tagRegex.exec(source)
    while (match) {
        var tag = createTag(match);
        rootTag.addChild(tag);
        if (tag.nest) {
            stack.push(rootTag);
            rootTag = tag;
        } else if (tag.end && rootTag.nest) {
            rootTag = stack.pop();
        }
        match = tagRegex.exec(source)
    }
    return rootTag;
}

// Convert any tags to their formatter counterparts
function createFormat (tag, source) {
    var start = tag.contentIndexStart;
    var end = tag.contentIndexStart;
    var contents = [];
    tag.children.forEach(child => {
      end = child.indexStart;
      if (end > start) {
          contents.push(source.substring(start, end));
      }

      var childFormat = createFormat(child, source);
      if (childFormat) {
          contents.push(childFormat);
      }
      start = end = child.indexEnd;
    });

    end = tag.contentIndexEnd;
    if (end > start) {
        contents.push(source.substring(start, end));
    }

    var format = TAGS[tag.name].format;
    if (format) {
        return new format(tag.options, contents);
    }
}

const format = (source) => {
    var rootTag = extractTags(source);
    return createFormat(rootTag, source);
}

const formatContent = (content, key) => {
  key = key || "text"; // default attribute storing display text

  // setup formatter cache
  if (!content._formatters) {
    content._formatters = {};
  }

  // setup formatter for this item
  if (!content._formatters[key]) {
    content._formatters[key] = format(content[key]);
  }

  // format the text
  return content._formatters[key].toString(content);
}

exports.format = format;
exports.formatContent = formatContent;
